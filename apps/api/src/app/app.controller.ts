import {
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  Patch,
  Put
} from '@nestjs/common';

import { AppService } from './app.service';
import { Observable } from 'rxjs';
import { todo } from '@demo-project/api-interfaces';
import TodoResponse = todo.TodoResponse;

@Controller('/')
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly logger: Logger
  ) {}

  @Get('ping')
  ping(){
      return {
        pong: 'pong'
      }
  }

  @Get()
  getTasks(): Observable<TodoResponse> {
    this.logger.log('catch get request');
    return this.appService.getTasks();
  }

  @Patch(':id/toggle')
  toggleDone(@Param('id') id): Observable<TodoResponse> {
    this.logger.log('catch patch request');
    return this.appService.toggleDone(id);
  }

  @Put()
  insertOne(@Body('title') label): Observable<TodoResponse> {
    this.logger.log('catch put request');
    return this.appService.insertOne(label);
  }

  @Delete(':id')
  delete(@Param('id') id): Observable<TodoResponse> {
    this.logger.log('catch delete request');
    return this.appService.delete(id);
  }
}
