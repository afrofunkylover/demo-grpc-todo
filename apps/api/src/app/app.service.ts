import { Injectable, OnModuleInit } from '@nestjs/common';
import { Client, ClientGrpc, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { todo } from '@demo-project/api-interfaces';
import TodoService = todo.TodoService;
import { environment } from '../environments/environment';


console.log('========================');
console.log(environment.grpc_url);
console.log('========================');

@Injectable()
export class AppService implements OnModuleInit {
  @Client({
    transport: Transport.GRPC,
    options: {
      url: environment.grpc_url,
      package: 'todoentity',
      protoPath: join(__dirname, './assets/todo.proto')
    }
  })
  client: ClientGrpc;

  todoService: TodoService;

  onModuleInit() {
    this.todoService = this.client.getService<TodoService>('Todo');
  }

  getTasks() {
    return this.todoService.getAll({});
  }

  toggleDone(id) {
    return this.todoService.toggle({ id });
  }

  insertOne(title) {
    return this.todoService.create({ title });
  }

  delete(id) {
    return this.todoService.delete({ id });
  }
}
