export const environment = {
  production: true,
  grpc_url: `${process.env.GRPC_HOST}:${process.env.GRPC_PORT}`
};
