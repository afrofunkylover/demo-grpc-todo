import { Component } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'demo-project-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  tasksList$ = this.appService.tasksList$;

  constructor(private appService: AppService) {}

  toggleTask(id) {
    this.appService.toggleDone(id);
  }

  onCreate(label) {
    this.appService.createNew(label);
  }

  deleteTask(id) {
    this.appService.delete(id);
  }
}
