import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { pluck, startWith, switchMap } from 'rxjs/operators';
import { todo } from '@demo-project/api-interfaces';
import TodoResponse = todo.TodoResponse;
import { environment } from '../environments/environment';


const baseUrl = environment.api_url;

@Injectable({ providedIn: 'root' })
export class AppService {
  private actions$ = new Subject<Observable<any>>();
  public tasksList$ = this.actions$.pipe(
    startWith(this.http.get(baseUrl + '/api')),
    switchMap(request => request),
    pluck('result')
  );

  constructor(private http: HttpClient) {}

  getList() {
    this.actions$.next(this.http.get<TodoResponse>(baseUrl + '/api'));
  }

  toggleDone(id) {
    this.actions$.next(this.http.patch<TodoResponse>(baseUrl + `/api/${id}/toggle`, {}));
  }

  createNew(title) {
    this.actions$.next(
      this.http.put<TodoResponse>(baseUrl + `/api`, { title })
    );
  }

  delete(id) {
    this.actions$.next(this.http.delete<TodoResponse>(baseUrl + `/api/${id}`));
  }
}
