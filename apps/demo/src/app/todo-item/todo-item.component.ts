import { Component, EventEmitter, Input, Output } from '@angular/core';
import { todo } from '@demo-project/api-interfaces';
import TodoItem = todo.TodoItem;

@Component({
  selector: 'demo-project-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss']
})
export class TodoItemComponent {
  @Input() task: TodoItem;
  @Output() toggle = new EventEmitter();
  @Output() delete = new EventEmitter();

  constructor() {}

  onDone() {
    this.toggle.emit(this.task.id);
  }

  onDelete() {
    this.delete.emit(this.task.id);
  }
}
