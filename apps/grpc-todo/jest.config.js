module.exports = {
  name: 'grpc-todo',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/grpc-todo'
};
