import { Controller, Logger } from '@nestjs/common';

import { AppService } from './app.service';
import { GrpcMethod } from '@nestjs/microservices';
import { todo } from '@demo-project/api-interfaces';
import TodoService = todo.TodoService;

@Controller()
export class AppController implements TodoService {
  constructor(
    private readonly appService: AppService,
    private readonly logger: Logger
  ) {}

  @GrpcMethod('Todo', 'Ping')
  Ping(data) {
    this.logger.log('catch ping');
    return {
      pong: 'pong'
    }
  }

  @GrpcMethod('Todo', 'Create')
  Create(data) {
    this.logger.log('Todo: Create catch');
    return this.appService.add(data);
  }

  @GrpcMethod('Todo', 'Toggle')
  Toggle(data) {
    this.logger.log('Todo: Toggle catch');
    return this.appService.toggle(data);
  }

  @GrpcMethod('Todo', 'Delete')
  Delete(data) {
    this.logger.log('Todo: Delete catch');
    return this.appService.delete(data);
  }
  @GrpcMethod('Todo', 'GetAll')
  GetAll() {
    this.logger.log('Todo: GetAll catch');
    return this.appService.get();
  }
}
