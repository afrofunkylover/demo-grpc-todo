import { Logger, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TodoSchema } from './schema/todo.schema';
import { environment } from '../environments/environment';

console.log('=====================================');

console.log(environment.mongo_url);

console.log('=====================================');

@Module({
  imports: [
    MongooseModule.forRoot(environment.mongo_url, { useNewUrlParser: true }),
    MongooseModule.forFeature([{ name: 'Todo', schema: TodoSchema }])
  ],
  controllers: [AppController],
  providers: [AppService, Logger]
})
export class AppModule {}
