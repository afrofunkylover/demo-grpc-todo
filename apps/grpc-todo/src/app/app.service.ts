import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Todo } from './todo.interface';
import { todo } from '@demo-project/api-interfaces';
import TodoResponse = todo.TodoResponse;

@Injectable()
export class AppService {
  constructor(@InjectModel('Todo') private readonly todoModel: Model<Todo>) {}

  async get(): Promise<TodoResponse> {
    const listOfModels = await this.todoModel.find({}).exec();
    // tslint:disable-next-line:no-shadowed-variable
    const result = listOfModels.map(({ title, id, isDone }) => ({
      title: `label: ${title}`,
      id,
      isDone
    }));
    return { result };
  }

  async add({ title }): Promise<TodoResponse> {
    const todoItem = new this.todoModel({ title, isDone: false });
    await todoItem.save();
    return this.get();
  }

  async delete({ id }) {
    const todoItem = await this.todoModel.findById(id);
    await todoItem.remove();
    return this.get();
  }

  async toggle({ id }) {
    const todoItem = await this.todoModel.findById(id);
    todoItem.isDone = !todoItem.isDone;
    await todoItem.save();
    return this.get();
  }
}
