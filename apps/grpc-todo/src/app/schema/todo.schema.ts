import { Schema } from 'mongoose';

export const TodoSchema = new Schema({
  title: String,
  isDone: Boolean
});
