export const environment = {
  production: true,
  mongo_url: process.env.MONGO_URI
};
