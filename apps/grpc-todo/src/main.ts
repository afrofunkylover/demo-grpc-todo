/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';
import { Transport } from '@nestjs/microservices';
import { join } from 'path';

async function bootstrap() {

  const url = process.env.PORT ? `0.0.0.0:${process.env.PORT}` : `localhost:6655`;
  console.log('======= ' + url + ' ===========');
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.GRPC,
    options: {
      url,
      package: 'todoentity',
      protoPath: join(__dirname, '/assets/todo.proto')
    }
  });
  await app.listenAsync();
}

bootstrap();
