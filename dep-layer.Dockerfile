FROM node:lts-alpine

COPY package*.json ./

RUN npm ci --only=prod
