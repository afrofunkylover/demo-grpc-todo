import { Observable } from 'rxjs';

export namespace todo {
  export interface CreateTodoDTO {
    title: string;
  }

  export interface TodoDTO {
    id: string;
  }

  export interface TodoResponse {
    result: TodoItem[];
  }

  export interface TodoItem {
    id: string;
    title: string;
    isDone: boolean;
  }

  export interface TodoService {
    Ping({}): {pong: string}
    Create(args: CreateTodoDTO): Promise<TodoResponse>;
    create?(args: CreateTodoDTO): Observable<TodoResponse>;
    Toggle(args: TodoDTO): Promise<TodoResponse>;
    toggle?(args: TodoDTO): Observable<TodoResponse>;
    Delete(args: TodoDTO): Promise<TodoResponse>;
    delete?(args: TodoDTO): Observable<TodoResponse>;
    GetAll({}): Promise<TodoResponse>;
    getAll?({}): Observable<TodoResponse>;
  }
}
