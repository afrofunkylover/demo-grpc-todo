#!/bin/bash -x

APP=$1
IMAGE_NAME=$2

function build_and_deploy()
{
  docker build -t $IMAGE_NAME -f apps/$APP/Dockerfile .
  docker push $IMAGE_NAME
}


[ -d dist/apps/$APP ] && build_and_deploy || echo "$APP Done"
